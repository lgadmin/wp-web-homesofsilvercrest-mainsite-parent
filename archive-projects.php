<?php
/**
 * The template for displaying all projects
 *
 * @package homebuilder
 */

get_header(); 
$page_title     = get_the_archive_title();
$title_type     = homebuilder_get_option( 'project_title_layout', 'one' );
$bg_id          = homebuilder_get_option( 'project_bg' );
$featured_image = wp_get_attachment_image_src( $bg_id, 'homebuilder-lg-soft' );;
$featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
set_query_var( 'page_title', $page_title ); 
set_query_var( 'featured_image', $featured_image ); 
get_template_part( "partials/page-title/{$title_type}" );
?>
<div class="p-v-xxl m-v-xxl">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            <?php
                $i = 0;
                if ( have_posts() ) {
                    while ( have_posts() ) {
                    the_post();
                    $i++;
                    $row_class   = ( ( $i % 2 ) == 0 ? 'recent_project_right' : '' ); 
                    $image_class = ( ( $i % 2 ) == 0 ? 'col-md-offset-4' : '' ); 
                    $categories  = get_the_terms( get_the_ID(), 'project-category' ); ?>
                    <div class="row recent_project <?php echo esc_attr( $row_class ); ?>">
                        <div class="col-md-8 <?php echo esc_attr( $image_class ); ?>">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail( 'homebuilder-md-soft' ); ?>
                            </a>
                        </div>
                        <div class="col-md-5 recent_project_details">
                            <div class="details_wrapper">
                                <div class="media">
                                    <div class="media-left media-middle p-r-md">
                                        <h1 class="text-white"><?php echo $i; ?></h1>
                                    </div>
                                    <div class="media-body text-dark">
                                        <?php 
                                        if ( !empty( $categories ) ) {
                                            echo homebuilder_get_project_categories( $categories );
                                        } ?>
                                        <h5 class="m-b-xs">
                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                } else { ?>
                    <h3><?php esc_html_e( 'No Entry Found!', 'homebuilder' );?></h3>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>