<?php
/**
* Search form template.
* 
* @package Homebuilder
*/
?>
<form role="search" method="get" class="form" action="<?php echo esc_url( home_url( '/' ) ); ?>" autocomplete="off">
    <div class="form-group">
        <div class="input-group">
            <label class="sr-only" for="search-form"><?php esc_html_e( 'Search for:', 'homebuilder' ) ?></label>
            <input type="search" class="form-control" id="search-form" placeholder="<?php esc_attr_e( 'Search', 'homebuilder' ) ?>" value="<?php echo get_search_query(true) ?>" name="s" title="<?php esc_attr_e( 'Search for:', 'homebuilder' ) ?>" />
            <span class="input-group-btn">
                <button class="btn btn-primary" type="submit" title="Search">
                    <span class="sr-only"><?php esc_html_e( 'Search', 'homebuilder' ) ?></span>
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>
</form>