<?php
/**
 * Template part for displaying video formated post.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Homebuilder
 */

$gallery = get_post_gallery( get_the_ID(), false );
$id_string = isset( $gallery['ids'] ) ? $gallery['ids'] : '';
$images = array_filter( explode( ',', $id_string ) );

if ( !empty( $images ) ) {
    echo '<div id="owlSingleImage" class="owl-carousel dots-inner">';
        foreach ( $images as $image ) {
             echo '<div class="item">' . wp_get_attachment_image( $image, 'homebuilder-md-hard' ) . '</div>';
        } 
    echo '</div>';
} else {
    get_template_part('partials/blog/content');
}