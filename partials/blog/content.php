<?php
/**
 * Default Template Part for Content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Homebuilder
 */
?>

<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'homebuilder-md-soft' ); ?></a>
