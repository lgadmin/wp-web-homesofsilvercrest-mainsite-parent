<?php
/**
 * Template part for displaying audio formated post.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Homebuilder
 */

$media = get_media_embedded_in_content( apply_filters( 'the_content', get_the_content() ) );

if ( isset( $media[0] ) ) {
    printf( '<div class="embed-responsive embed-responsive-16by9">%1$s</div>', $media[0] );
} else {
   get_template_part('partials/blog/content');
}