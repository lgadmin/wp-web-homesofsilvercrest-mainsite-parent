<section class="img-bg img-dark att-fixed" style="background-image: url(<?php echo esc_url( $featured_image ); ?>);">
    <div class="container">
        <div class="row p-v-xxl m-v-xxl">
            <div class="col-md-12 page_heading">
                <h1 class="title text-xl font-light text-primary"><?php echo esc_html( $page_title ); ?></h1>
            </div>
        </div>
    </div>
</section>
<section class="bg-light p-v-lg b-b">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php homebuilder_breadcrumbs(); ?>
            </div>
        </div>
    </div>
</section>