<section class="img-bg img-dark att-fixed" style="background-image: url(<?php echo esc_url( $featured_image ); ?>);">
    <div class="container">
        <div class="row p-v-xxl m-v-xxl">
            <div class="col-md-12">
                <h4 class="l-s-2x pull-left"><?php echo esc_html( $page_title ); ?></h4>
                <div class="pull-right m-t-xxs">
                    <?php homebuilder_breadcrumbs(); ?>
                </div>
            </div>
        </div>
    </div>
</section>