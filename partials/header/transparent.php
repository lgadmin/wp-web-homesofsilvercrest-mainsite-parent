<?php
$header_data = get_query_var('header_data');
extract( $header_data );
?>

<header class="headroomTransparent navbar-fixed-top bg-transparent text-white p-v-md">
    <div class="container">
        <div class="row">
            <div class="navbar-header">
                <a class="btn_menu visible-xs visible-sm pull-right text-lg" data-toggle="collapse" data-target=".mainNav">
                <i class="fa fa-bars"></i></a>
                <?php echo homebuilder_get_logo( $logo, $retina ); ?>
            </div>
            <?php homebuilder_get_primary_nav(); ?>
        </div>
    </div>
</header>