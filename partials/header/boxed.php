<?php
$header_data = get_query_var('header_data');
extract( $header_data );

if ( $has_topbar ) { ?>
    <section class="header_top bg-primary hidden-sm hidden-xs">
        <div class="container">
            <div class="row text-dark m-b-sm">
                <div class="col-md-4">
                    <p class="toper_socials m-b-none">
                    <?php
                        if ( $has_social ) { homebuilder_social_media(); }
                        if ( $has_tagline ) { echo '<span class="m-h text-sm">' . $tagline_content . '</span>'; } ?>
                    </p>
                </div>
                <div class="col-md-8 text-right">
                    <?php homebuilder_topbar_right( $phone, $email, $time ); ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>

<header class="header_container headroom">
    <div class="container bg-white p-v-md">
        <div class="row">
            <div class="navbar-header">
                <a class="btn_menu visible-xs visible-sm pull-right text-lg" data-toggle="collapse" data-target=".mainNav">
                <i class="fa fa-bars"></i></a>
                <?php echo homebuilder_get_logo( $logo, $retina ); ?>
            </div>
            <?php homebuilder_get_primary_nav(); ?>
        </div>
    </div>
</header>