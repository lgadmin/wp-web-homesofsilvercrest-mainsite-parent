<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package homebuilder
 */ 
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div class="windowscreen error-404 not-found" style="height:100vh">
	    <div class="item">
	        <div class="center">
	            <div class="container">
	                <div class="row">
	                    <div class="col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3 text-center">
	                        <img class="img-full" src="<?php echo get_template_directory_uri(); ?>/assets/imgs/404.jpg" alt="<?php esc_html_e( '404 Not Found!', 'homebuilder' ); ?>">
	                        <a href="<?php echo esc_url( home_url('/') ); ?>" class="btn btn-default m-t-xl"><?php esc_html_e( 'Back to Home', 'homebuilder' ); ?></a>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php wp_footer(); ?>
</body>
</html>

