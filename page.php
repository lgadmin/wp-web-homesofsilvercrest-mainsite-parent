<?php
/**
 * The template for displaying default page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package homebuilder
 */

get_header();
$page_title     = get_the_title();
$sidebar        = homebuilder_get_option( 'page_sidebar', 'right' );
$title_type     = homebuilder_get_option( 'page_title_layout', 'one' );
$column         = homebuilder_get_column_class( $sidebar );
$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( get_queried_object_id() ), 'homebuilder-lg-soft' );
$featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
set_query_var( 'page_title', $page_title ); 
set_query_var( 'featured_image', $featured_image ); 
get_template_part( "partials/page-title/{$title_type}" );
?>
<section class="p-v-xxl">
    <div class="container page-content page-content-<?php the_ID(); ?>">
        <div class="row">
            <div class="<?php echo esc_attr( $column['main'] ); ?> m-b-xxl">
                <?php
                if ( have_posts() ) { 
                    while ( have_posts() ) { the_post(); $categories = get_the_category(); ?>
                        <article id="page-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
                            <div class="p-v-xl">
                                <?php
                                the_content();

                                wp_link_pages( array(
                                    'before' => '<ul class="pagination pagination-md"><li class="page-numbers">',
                                    'after' => '</li></ul>',
                                    'separator' => '</li><li class="page-numbers">'
                                ));
                                ?>
                                <div class="">
                                	<?php 
                                	// If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    } ?>
                                </div>
                            </div>
                        </article>
                    <?php } #endwhile ?>
                <?php } #endif
                else { esc_html_e( 'Nothing Found', 'homebuilder' ); } ?>
            </div>
            <?php if ( $sidebar !== 'no-sidebar') { ?>
                <div class="<?php echo esc_attr( $column['sidebar'] ); ?> m-b-xxl">
                    <?php get_sidebar(); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>
