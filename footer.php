<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/
 *
 * @package homebuilder
 */

$footer_widget_area_columns = absint( homebuilder_get_option( 'footer_widget_area_columns', 3 ) );
$display_footer_copyright   = homebuilder_get_option( 'display_footer_copyright', true );
$footer_copyright_content   = homebuilder_get_option( 'footer_copyright_content' );
$display_footer = false;
for ($id=0; $id <= $footer_widget_area_columns; $id++) { 
    if ( is_active_sidebar("footer-{$id}") ) {
        $display_footer = true;
    }
}
if ( $display_footer ) { ?>
    <div class="p-t-xxl bg-dark">
        <div class="container">
            <div class="row">
                <?php for ( $i=1; $i<=$footer_widget_area_columns; $i++ ) { ?>
                    <div class="col-md-<?php echo esc_attr( 12/$footer_widget_area_columns ); ?> m-b-xxl"><?php dynamic_sidebar( "footer-{$i}" ); ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php }
if ( $display_footer_copyright ) {
    echo '<footer class="p-v-lg text-sm bg-black text-center">';
    if ( !empty( $footer_copyright_content ) ) {
        echo $footer_copyright_content;
    } else {
        printf( esc_html__( 'Copyright &copy; %s %s - Crafted with all the love in the world by %s', 'homebuilder' ), date( 'Y' ), get_bloginfo( 'name' ), '<a href="http://themebucket.net">ThemeBucket</a>' );
    }
    echo '</footer>';
}

wp_footer(); ?>

</body>
</html>

