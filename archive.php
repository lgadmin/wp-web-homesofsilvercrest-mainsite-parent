<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package homebuilder
 */

get_header();
$page_title     = homebuilder_get_option( 'blog_title', esc_html__( 'BLOG', 'homebuilder' ) );
$sidebar        = homebuilder_get_option( 'blog_sidebar', 'right' );
$date_format    = homebuilder_get_option( 'date_format', 'F j, Y ' );
$title_type     = homebuilder_get_option( 'post_title_layout', 'one' );
$column         = homebuilder_get_column_class( $sidebar );
$blgo_bg_id     = homebuilder_get_option( 'blog_bg' );
$featured_image = wp_get_attachment_image_src( $blgo_bg_id, 'homebuilder-lg-soft' );
$featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
set_query_var( 'page_title', $page_title ); 
set_query_var( 'featured_image', $featured_image ); 
get_template_part( "partials/page-title/{$title_type}" );
?>
<section class="p-v-xxl">
    <div class="container">
        <div class="row">
            <div class="<?php echo esc_attr( $column['main'] ); ?> m-b-xxl">

                <?php
                if ( have_posts() ) { 
                    while ( have_posts() ) { the_post(); $categories = get_the_category(); ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'm-b-xxl' ); ?>>
                        <div class="graf_figure">
                            <?php get_template_part( 'partials/blog/content', get_post_format() ); ?>
                        </div>
                        <div class="p-v-xl">
                            <span class=""><a href="<?php echo the_permalink(); ?>"><?php the_time( $date_format ); ?></a></span>
                            <div class="section_heading m-t-sm">
                                <h1 class="title">
                                    <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                                </h1>
                                <div class="sub_title text-md m-t-sm m-b-xl">
                                    <?php
                                        //retrive author link
                                    echo homebuilder_author_link(); 
                                        //retrive categories
                                    if ( !empty( $categories ) ) {  homebuilder_get_post_categories( $categories ); };
                                        //retrive comments number
                                    homebuilder_post_comments_number(); 
                                    ?>
                                </div>
                            </div>

                            <div><?php the_excerpt(); ?></div>
                        </div>
                    </article>
                    <?php } #endwhile ?>

                    <nav>
                        <!--pagination -->
                        <?php homebuilder_posts_navigation(); ?>
                    </nav>

                <?php } else { ?>
                    <h3><?php esc_html_e( 'No Entry Found!', 'homebuilder' );?></h3>
                <?php } ?>

                </div>

            <?php if ( $sidebar !== 'no-sidebar') { ?>
            <div class="<?php echo esc_attr( $column['sidebar'] ); ?> m-b-xxl">
                <?php get_sidebar(); ?>
            </div>
            <?php } ?>

        </div>
    </div>
</section>
<?php get_footer(); ?>
