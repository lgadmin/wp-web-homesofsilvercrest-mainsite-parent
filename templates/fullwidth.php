<?php
/**
 * Template Name: Fullwidth
 * 
 * The template for displaying fullwidth page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package homebuilder
 */

get_header();
$page_title     = get_the_title();
$title_type     = homebuilder_get_option( 'page_title_layout', 'one' );
$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'homebuilder-lg-soft' );
$featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
set_query_var( 'page_title', $page_title ); 
set_query_var( 'featured_image', $featured_image ); 
get_template_part( "partials/page-title/{$title_type}" );
?>
<section class=""> <!--dynamic class will be added here, depends of header layout-->
    <div class="container-fluid page-content page-content-<?php the_ID(); ?>">
        <?php
        if ( have_posts() ) { 
            while ( have_posts() ) { the_post(); $categories = get_the_category(); ?>
                <article id="page-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>
                    <?php the_content(); ?>
                    <div class="">
                        <?php 
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        } ?>
                    </div>
                </article>
            <?php } #endwhile ?>
        <?php } #endif
        else { esc_html_e( 'Nothing Found', 'homebuilder' ); } ?>
    </div>
</section>
<?php get_footer(); ?>
