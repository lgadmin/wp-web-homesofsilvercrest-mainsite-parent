<?php
/**
 * The template for displaying all single project.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package homebuilder
 */

get_header(); 
$page_title     = get_the_title();
$title_type     = homebuilder_get_option( 'page_title_layout', 'one' );
$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'homebuilder-lg-soft' );
$featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
set_query_var( 'page_title', $page_title ); 
set_query_var( 'featured_image', $featured_image ); 
get_template_part( "partials/page-title/{$title_type}" );
?>
<section class="post-content-<?php the_ID(); ?>">
	<?php
	while ( have_posts() ) { the_post(); ?>
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div><?php the_content(); ?></div>
                    </article>
                    <div>
                    	<?php 
                    	// If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php } // End of the loop. ?>
</section> <!--.post-content<?php the_ID(); ?> -->
<?php get_footer(); ?>