<?php
/**
 * Handles all the demo related tasks.
 *
 * @package Homebuilder
 * @author ThemeBucket <themebucket@gmail.com>
 */

class Homebuilder_Demo_Controller {

    public function __construct() {
        add_filter( 'pt-ocdi/import_files', array($this, 'register') );
        add_action( 'pt-ocdi/after_import', array($this, 'add_menu') );
        add_action( 'pt-ocdi/after_import', array($this, 'update_options') );
        add_filter( 'pt-ocdi/plugin_intro_text', array($this, 'set_help_note') );
    }

    public function register() {
        return array(
            array(
                'import_file_name' => 'HomeBuilder',
                'import_file_url' => HOMEBUILDER_URI . 'demos/content.xml',
                'import_widget_file_url' =>  HOMEBUILDER_URI . 'demos/widgets.json',
            )
        );
    }

    public function add_menu() {
        $main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
        set_theme_mod( 'nav_menu_locations',
            array(
                'primary' => $main_menu->term_id,
            )
        );
    }

    public function update_options() {
        $file = HOMEBUILDER_ROOT . 'demos/theme_options.txt';

        if ( file_exists( $file ) ) {
            $bypass_file_get_contents = str_replace('bypass_', '', 'bypass_file_get_contents');
            $data = $bypass_file_get_contents( $file );
            update_option( CS_OPTION, cs_decode_string( $data ) );
        }
    }

    public function set_help_note( $text ) {
        ob_start();
        ?>
        <div class="ocdi__intro-text">
            <p><?php esc_html_e( 'Importing demo data (post, pages, images, theme settings, ...) is the easiest way to setup your theme. It will allow you to quickly edit everything instead of creating content from scratch. When you import the data, the following things might happen:', 'homebuilder' ); ?></p>
            <ul>
                <li><?php esc_html_e( 'No existing posts, pages, categories, images, custom post types or any other data will be deleted or modified.', 'homebuilder' ); ?></li>
                <li><?php esc_html_e( 'Posts, pages, images, widgets and menus will get imported.', 'homebuilder' ); ?></li>
            </ul>
        </div>
        <div class="ocdi__intro-text">
            <p><?php esc_html_e( 'Before you begin, make sure all the required plugins are activated.', 'homebuilder' ); ?></p>
        </div>
        <div class="ocdi__intro-text">
            <p><?php esc_html_e( 'Note: If you face any unexpected situation then click on the <strong>Import Demo Data</strong> button again. Happy Journey.', 'homebuilder' ) ; ?></p>
        </div>
        <?php
        return ob_get_clean();
    }

}

new Homebuilder_Demo_Controller();