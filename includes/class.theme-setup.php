<?php 

class Homebuilder_Theme_Setup {

    public function __construct() {
        add_action( 'after_setup_theme', array($this, 'init') );
    }

    /**
     * Execute all methods
     * @return void
     */
    public function init(){
        $this->load_text_domain();
        $this->add_theme_supports();
        $this->add_image_sizes();
        $this->register_nav_menus();
        $this->set_content_width();
    }

    /**
     * Load theme text domain
     * @return void
     */
    protected function load_text_domain() {
        load_theme_textdomain( 'homebuilder', get_template_directory() . '/languages' );
    }

    /**
     * Set global content width for 3rd party and media uses
     * @return void
     */
    protected function set_content_width() {
        $GLOBALS['content_width'] = apply_filters( 'homebuilder_content_width', 750 );
    }

    /**
     * Set which functionalites homebuilder supports
     * @return void
     */
    protected function add_theme_supports() {
        add_theme_support( 'automatic-feed-links' );

        add_theme_support( 'title-tag' );

        add_theme_support( 'post-thumbnails' );

        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ) );

        add_theme_support( 'post-formats', array(
            'gallery',
            'audio',
            'video',
        ) );
    }

    /**
     * Add homebuilder image sizes
     * @return void
     */
    protected function add_image_sizes() {
        add_image_size( 'homebuilder-sm-hard', 555, 444, true );
        add_image_size( 'homebuilder-sm-soft', 555, 999999 );
        add_image_size( 'homebuilder-md-hard', 750, 400, true );
        add_image_size( 'homebuilder-md-soft', 750, 999999 );
        add_image_size( 'homebuilder-lg-soft', 1400, 999999 );
    }

    /**
     * Register default nav menus
     * @return void
     */
    protected function register_nav_menus() {
        register_nav_menus( array(
            'primary' => esc_html__( 'Primary Menu', 'homebuilder' ),
        ) );
    }

}

new Homebuilder_Theme_Setup;