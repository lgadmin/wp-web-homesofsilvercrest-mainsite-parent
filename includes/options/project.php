<?php
/**
 * Project
 */
$homebuilder_options[] = array(
    'name'     => 'homebuilder-project',
    'title'    => esc_html__( 'Project', 'homebuilder' ),
    'icon'     => 'fa fa-building-o',
    'fields' => array(
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Projects Archive', 'homebuilder' ),
            ),
        array(
            'id'      => 'project_title_layout',
            'type'    => 'image_select',
            'title'   => esc_html__( 'Title Layout', 'homebuilder' ),
            'desc'    => esc_html__( 'Select title layout', 'homebuilder' ),
            'default' => 'one',
            'radio'   => true,
            'options' => array(
                'one' => esc_url( $imgs . 'page-title-1.jpg' ),
                'two' => esc_url( $imgs . 'page-title-2.jpg' )
                ),
            ),
        array(
            'id'         => 'project_bg',
            'type'       => 'image',
            'title'      => esc_html__( 'Title Backgroud', 'homebuilder' ),
            'desc'       => esc_html__( 'Select an image as a background image for page title.', 'homebuilder' ),
            ),
        )
    );
