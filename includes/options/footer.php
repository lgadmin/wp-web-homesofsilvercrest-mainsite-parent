<?php
$homebuilder_options[] = array(
    'name'   => 'homebuilder-footer',
    'title'  => esc_html__( 'Footer', 'homebuilder' ),
    'icon'   => 'fa fa-bars',
    'fields' => array(
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Footer Widget Area', 'homebuilder' ),
            ),
        array(
            'id'         => 'footer_widget_area_columns',
            'type'       => 'select',
            'title'      => esc_html__( 'Widget Area Columns', 'homebuilder' ),
            'default'    => 3,
            'desc'       => esc_html__( 'Select number of columns for footer widget area. ', 'homebuilder' ),
            'options'    => array(
                '1' => esc_html__( 'Column 1', 'homebuilder' ),
                '2' => esc_html__( 'Column 2', 'homebuilder' ),
                '3' => esc_html__( 'Column 3', 'homebuilder' ),
                '4' => esc_html__( 'Column 4', 'homebuilder' ),
                )
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Copyright', 'homebuilder' ),
            ),
        array(
            'id'      => 'display_footer_copyright',
            'type'    => 'switcher',
            'title'   => esc_html__( 'Display Copyright', 'homebuilder' ),
            'default' => true,
            'desc'    => esc_html__( 'Switch on to display copyright information.', 'homebuilder' ),
            ),
        array(
            'id'         => 'footer_copyright_content',
            'type'       => 'textarea',
            'title'      => esc_html__( 'Copyright Content', 'homebuilder' ),
            'desc'       => esc_html__( 'Add copyright content here.', 'homebuilder' ),
            'dependency' => array( 'display_footer_copyright', '==', true ),
            ),
        ),
    );
