<?php
/*
 * General
 */
$homebuilder_options[] = array(
    'name'   => 'homebuilder-General',
    'title'  => esc_html__( 'General', 'homebuilder' ),
    'icon'   => 'fa fa-globe',
    'fields' => array(
        array(
            'id'      => 'display_preloader',
            'type'    => 'switcher',
            'title'   => esc_html__( 'Display Preloader', 'homebuilder' ),
            'desc'    => esc_html__( 'Switch on to display preloader.', 'homebuilder' ),
            'default' => true
            ),
        array(
            'id'      => 'display_backtotop',
            'type'    => 'switcher',
            'title'   => esc_html__( 'Display BackToTop', 'homebuilder' ),
            'desc'    => esc_html__( 'Switch on to display back to top.', 'homebuilder' ),
            'default' => true
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Color Scheme', 'homebuilder' ),
            ),
        array(
            'id'    => 'custom_brand_color',
            'type'  => 'switcher',
            'title' => esc_html__( 'Custom Theme Color', 'homebuilder' ),
            'desc'  => esc_html__( 'Switch on to customize theme color.', 'homebuilder' ),
            ),
        array(
            'id'    => 'brand_color',
            'type'  => 'color_picker',
            'title' => esc_html__( 'Theme Color', 'homebuilder' ),
            'desc'  => esc_html__( 'Select an amazing theme color from unlimited posibilites.', 'homebuilder' ),
            'default' => '#ffd460',
            'dependency' => array('custom_brand_color', '==', true)
            ),
        ),
    );
