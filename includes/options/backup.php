<?php
/**
 * Backup
 */
$homebuilder_options[] = array(
    'name'   => 'backup',
    'title'  => esc_html__( 'Backup', 'homebuilder' ),
    'icon'   => 'fa fa-suitcase',
    'fields' => array(
        array(
            'id'    => 'backup',
            'type'  => 'backup',
            'title' => esc_html__( 'Backup Options', 'homebuilder' ),
            )
        )
    );