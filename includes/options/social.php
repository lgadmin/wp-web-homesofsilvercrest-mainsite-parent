<?php
/**
 * Social Media
 */
$homebuilder_social_links = array(
    'behance'        => esc_html__( 'Behance', 'homebuilder' ),
    'bitbucket'      => esc_html__( 'Bitbucket', 'homebuilder' ),
    'custom'         => esc_html__( 'Custom', 'homebuilder' ),
    'deviantart'     => esc_html__( 'Deviant Art', 'homebuilder' ),
    'dribbble'       => esc_html__( 'Dribbble', 'homebuilder' ),
    'email'          => esc_html__( 'Email', 'homebuilder' ),
    'facebook'       => esc_html__( 'Facebook', 'homebuilder' ),
    'flickr'         => esc_html__( 'Flickr', 'homebuilder' ),
    'github'         => esc_html__( 'Github', 'homebuilder' ),
    'google-plus'    => esc_html__( 'Google Plus', 'homebuilder' ),
    'instagram'      => esc_html__( 'Instagram', 'homebuilder' ),
    'jsfiddle'       => esc_html__( 'JsFiddle', 'homebuilder' ),
    'linkedin'       => esc_html__( 'LinkedIn', 'homebuilder' ),
    'medium'         => esc_html__( 'Medium', 'homebuilder' ),
    'pinterest'      => esc_html__( 'Pinterest', 'homebuilder' ),
    'slideshare'     => esc_html__( 'Slide Share', 'homebuilder' ),
    'soundcloud'     => esc_html__( 'Sound Cloud', 'homebuilder' ),
    'stack-exchange' => esc_html__( 'Stack Exchange', 'homebuilder' ),
    'stack-overflow' => esc_html__( 'Stack Overflow', 'homebuilder' ),
    'tumblr'         => esc_html__( 'Tumblr', 'homebuilder' ),
    'twitter'        => esc_html__( 'Twitter', 'homebuilder' ),
    'vimeo'          => esc_html__( 'Vimeo', 'homebuilder' ),
    'whatsapp'       => esc_html__( 'WhatsApp', 'homebuilder' ),
    'youtube'        => esc_html__( 'Youtube', 'homebuilder' ),
);

$homebuilder_options[] = array(
    'name'   => 'homebuilder-social-media',
    'title'  => esc_html__( 'Social Media', 'homebuilder' ),
    'icon'   => 'fa fa-bullhorn',
    'fields' => array(
        array(
            'id'              => 'social_media',
            'type'            => 'group',
            'title'           => esc_html__( 'Social Media', 'homebuilder' ),
            'button_title'    => esc_html__( 'Add New Media', 'homebuilder' ),
            'accordion_title' => esc_html__( 'Media Name', 'homebuilder' ),
            'fields'          => array(
                array(
                    'id'      => 'name',
                    'type'    => 'select',
                    'title'   => esc_html__( 'Media', 'homebuilder' ),
                    'options' => $homebuilder_social_links
                    ),
                array(
                    'id'         => 'icon-name',
                    'type'       => 'text',
                    'title'      => esc_html__( 'Custom Icon', 'homebuilder' ),
                    'desc'       => esc_html__( 'Add a Font Awesome icon class. Note: class name from different icon library may not work properly.', 'homebuilder' ),
                    'dependency' => array('name', '==', 'custom'),
                    ),
                array(
                    'id'    => 'link',
                    'type'  => 'text',
                    'title' => esc_html__( 'Link', 'homebuilder' ),
                    )
                )
            )
        )
    );
