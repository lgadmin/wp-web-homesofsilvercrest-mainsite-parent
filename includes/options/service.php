<?php
$homebuilder_options[] = array(
    'name'   => 'homebuilder-service',
    'title'  => esc_html__( 'Service', 'homebuilder' ),
    'icon'   => 'fa fa-briefcase',
    'fields' => array(
        array(
            'id'      => 'service_title_layout',
            'type'    => 'image_select',
            'title'   => esc_html__( 'Title Layout', 'homebuilder' ),
            'desc'    => esc_html__( 'Select service page title layout', 'homebuilder' ),
            'default' => 'one',
            'radio'   => true,
            'options' => array(
                'one' => esc_url( $imgs . 'page-title-1.jpg' ),
                'two' => esc_url( $imgs . 'page-title-2.jpg' )
                ),
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Service Logo', 'homebuilder' ),
            ),
        array(
            'id'         => 'service_logo',
            'type'       => 'image',
            'title'      => esc_html__( 'Service Logo', 'homebuilder' ),
            'desc'       => esc_html__( 'Select an image as service logo.', 'homebuilder' ),
            ),
        ),
    );
