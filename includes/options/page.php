<?php
$homebuilder_options[] = array(
    'name'   => 'homebuilder-page',
    'title'  => esc_html__( 'Page', 'homebuilder' ),
    'icon'   => 'fa fa-file-o',
    'fields' => array(
        array(
            'id'      => 'page_title_layout',
            'type'    => 'image_select',
            'title'   => esc_html__( 'Title Layout', 'homebuilder' ),
            'desc'    => esc_html__( 'Select page title layout', 'homebuilder' ),
            'default' => 'one',
            'radio'   => true,
            'options' => array(
                'one' => esc_url( $imgs . 'page-title-1.jpg' ),
                'two' => esc_url( $imgs . 'page-title-2.jpg' )
                ),
            ),
        array(
            'id'         => 'page_sidebar',
            'type'       => 'radio',
            'title'      => esc_html__( 'Sidebar Layout', 'homebuilder' ),
            'desc'       => esc_html__( 'Select page sidebar layout.', 'homebuilder' ),
            'default'    => 'right',
            'options'    => array(
                'left'       => esc_html__( 'Sidebar Left', 'homebuilder'),
                'right'      => esc_html__( 'Sidebar Right', 'homebuilder'),
                'no-sidebar' => esc_html__( 'No Sidebar', 'homebuilder'),
                ),
            ),
        ),
    );
