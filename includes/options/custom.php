<?php

$homebuilder_options[] = array(
    'name'     => 'homebuilder-custom-code',
    'title'    => esc_html__( 'Custom Code', 'homebuilder' ),
    'icon'     => 'fa fa-cogs',
    'sections' => array(
        array(
            'name'   => 'homebuilder-css',
            'title'  => esc_html__( 'CSS', 'homebuilder' ),
            'icon'   => 'fa fa-code',
            'fields' => array(
                array(
                    'type'    => 'subheading',
                    'content' => esc_html__( 'You do not need to add "style" tag in this field. Your JavaScript code will be wrapped using "style" tag automatically and then will be added to your site\'s header', 'homebuilder' ),
                    ),
                array(
                    'id'         => 'homebuilder_css',
                    'type'       => 'textarea',
                    'sanitize'   => false,
                    'attributes' => array(
                        'style' => 'height:400px',
                        ),
                    )
                )
            ),
        array(
            'name'   => 'homebuilder-js',
            'title'  => esc_html__( 'JavaScript', 'homebuilder' ),
            'icon'   => 'fa fa-code',
            'fields' => array(
                array(
                    'type'    => 'subheading',
                    'content' => esc_html__( 'You do not need to add "script" tag in this field. Your JavaScript code will be wrapped using "script" tag automatically and then will be added to your site\'s footer', 'homebuilder' ),
                    ),
                array(
                    'id'         => 'homebuilder_js',
                    'type'       => 'textarea',
                    'sanitize'   => false,
                    'attributes' => array(
                        'style' => 'height:400px',
                        ),
                    )
                )
            ),
        )
    );
