<?php
/**
 * Blog
 */
$homebuilder_options[] = array(
    'name'     => 'homebuilder-blog',
    'title'    => esc_html__( 'Blog', 'homebuilder' ),
    'icon'     => 'fa fa-comments',
    'fields' => array(
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Blog Title', 'homebuilder' ),
            ),
        array(
            'id'      => 'blog_title',
            'type'    => 'text',
            'title'   => esc_html__( 'Blog Title', 'homebuilder' ),
            'desc'    => esc_html__( 'Change blog page title', 'homebuilder' ),
            'default' => esc_html__( 'BLOG', 'homebuilder' ),
            ),
        array(
            'id'      => 'post_title_layout',
            'type'    => 'image_select',
            'title'   => esc_html__( 'Title Layout', 'homebuilder' ),
            'desc'    => esc_html__( 'Select blog title layout', 'homebuilder' ),
            'default' => 'one',
            'radio'   => true,
            'options' => array(
                'one' => esc_url( $imgs . 'page-title-1.jpg' ),
                'two' => esc_url( $imgs . 'page-title-2.jpg' )
                ),
            ),
        array(
            'id'         => 'blog_bg',
            'type'       => 'image',
            'title'      => esc_html__( 'Title Backgroud', 'homebuilder' ),
            'desc'       => esc_html__( 'Select an image as a background image for blog page title.', 'homebuilder' ),
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Blog Layout', 'homebuilder' ),
            ),
        array(
            'id'         => 'blog_sidebar',
            'type'       => 'radio',
            'title'      => esc_html__( 'Sidebar', 'homebuilder' ),
            'desc'       => esc_html__( 'Select blog sidebar', 'homebuilder' ),
            'default'    => 'right',
            'options'    => array(
                'left'       => esc_html__( 'Sidebar Left', 'homebuilder'),
                'right'      => esc_html__( 'Sidebar Right', 'homebuilder'),
                'no-sidebar' => esc_html__( 'No Sidebar', 'homebuilder'),
                ),
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Post Date Format', 'homebuilder' ),
            ),
        array(
            'id'      => 'date_format',
            'type'    => 'text',
            'title'   => esc_html__( 'Date Format', 'homebuilder' ),
            'default' => 'F j, Y',
            'desc'    => esc_html__( 'Customize your post date format.', 'homebuilder'),
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Read More', 'homebuilder' ),
            ),
        array(
            'id'         => 'read_more_text',
            'type'       => 'text',
            'title'      => esc_html__( 'Custom Text', 'homebuilder' ),
            'default'    => esc_html__( 'Read More', 'homebuilder' ),
            'desc'       => esc_html__( 'Change read more button text.', 'homebuilder')
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Post Exceprt', 'homebuilder' ),
            ),
        array(
            'id'         => 'excerpt_length',
            'type'       => 'text',
            'title'      => esc_html__( 'Excerpt Length', 'homebuilder' ),
            'default'    => 50,
            'desc'       => esc_html__( 'Change post excerpt length', 'homebuilder'),
            ),
        )
    );
