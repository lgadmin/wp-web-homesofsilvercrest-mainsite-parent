<?php
$homebuilder_options[] = array(
    'name'   => 'homebuilder-header',
    'title'  => esc_html__( 'Header', 'homebuilder' ),
    'icon'   => 'fa fa-bars',
    'fields' => array(
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Main Logo', 'homebuilder' ),
            ),
        array(
            'id'         => 'logo_default',
            'type'       => 'image',
            'title'      => esc_html__( 'Default Logo', 'homebuilder' ),
            'desc'       => esc_html__( 'Select an image for logo.', 'homebuilder' ),
            ),
        array(
            'id'         => 'logo_retina',
            'type'       => 'image',
            'title'      => esc_html__( 'Retina Logo', 'homebuilder' ),
            'desc'       => esc_html__( 'Select an image for the retina version of the logo. It should be exactly 2x the size of main logo.', 'homebuilder' ),
            ),
        array(
            'type'    => 'subheading',
            'content' => esc_html__( 'Header', 'homebuilder' )
            ),
        array(
            'id'         => 'header_layout',
            'type'       => 'image_select',
            'title'      => esc_html__( 'Layout', 'homebuilder' ),
            'default'    => 'default',
            'radio'      => true,
            'attributes' => array(
                'data-depend-id' => 'header_layout',
            ),
            'options' => array(
                'default'     => esc_url( $imgs . 'header-default.jpg' ),
                'boxed'       => esc_url( $imgs . 'header-boxed.jpg' ),
                'transparent' => esc_url( $imgs . 'header-transparent.jpg' ),
                ),
            ),
        array(
            'type'       => 'subheading',
            'content'    => esc_html__( 'Top Bar', 'homebuilder' ),
            'dependency' => array( 'header_layout', 'any', 'default,boxed' ),
            ),
        array(
            'id'         => 'has_topbar',
            'type'       => 'switcher',
            'title'      => esc_html__( 'Display Top Bar', 'homebuilder' ),
            'default'    => true,
            'desc'       => esc_html__( 'Switch on to display top bar', 'homebuilder' ),
            'dependency' => array( 'header_layout', 'any', 'default,boxed' )
            ),
        array(
            'id'         => 'display_topbar_social',
            'type'       => 'switcher',
            'title'      => esc_html__( 'Display Social Icons', 'homebuilder' ),
            'default'    => true,
            'desc'       => esc_html__( 'Switch on to display top header social icons', 'homebuilder' ),
            'dependency' => array( 'header_layout|has_topbar', 'any|==', 'default,boxed|true' )
            ),
        array(
            'id'         => 'display_topbar_tagline',
            'type'       => 'switcher',
            'title'      => esc_html__( 'Display Tagline', 'homebuilder' ),
            'default'    => true,
            'desc'       => esc_html__( 'Switch on to display top header tagline', 'homebuilder' ),
            'dependency' => array( 'header_layout|has_topbar', 'any|==', 'default,boxed|true' )
            ),
        array(
            'id'         => 'topbar_tagline_content',
            'type'       => 'textarea',
            'title'      => esc_html__( 'Tagline Content', 'homebuilder' ),
            'desc'       => esc_html__( 'Add tagline content here.', 'homebuilder' ),
            'default'    => get_bloginfo( 'description' ),
            'dependency' => array( 'header_layout|has_topbar|display_topbar_tagline', 'any|==|==', 'default,boxed|true|true' ),
            ),
        array(
            'id'         => 'topbar_phone_no',
            'type'       => 'text',
            'title'      => esc_html__( 'Contact Number', 'homebuilder' ),
            'desc'       => esc_html__( 'Add contact number to display top header', 'homebuilder' ),
            'dependency' => array( 'header_layout|has_topbar', 'any|==', 'default,boxed|true' )
            ),
        array(
            'id'         => 'topbar_email_addr',
            'type'       => 'text',
            'title'      => esc_html__( 'Contact Email', 'homebuilder' ),
            'desc'       => esc_html__( 'Add contact email to display top header', 'homebuilder' ),
            'dependency' => array( 'header_layout|has_topbar', 'any|==', 'default,boxed|true' ),
            ),
        array(
            'id'         => 'topbar_available_time',
            'type'       => 'text',
            'title'      => esc_html__( 'Contact Time Schedule', 'homebuilder' ),
            'desc'       => esc_html__( 'Add contact time schedule to display top header', 'homebuilder' ),
            'dependency' => array( 'header_layout|has_topbar', 'any|==', 'default,boxed|true' )
            ),
        ),
    );
