<?php
/**
 * This file contains all the required and recommended plugin list.
 *
 * @package HomeBuilder
 * @author ThemeBucket <themebucket@gmail.com>
 */

/** TGM Activation added */
require_once HOMEBUILDER_VENDORS_DIR . 'class-tgm-plugin-activation.php';

class HomeBuilder_Required_Plugins {

    public function __construct() {
        add_action( 'tgmpa_register', array($this, 'register') );
    }

    public function register() {
        tgmpa( $this->get_plugins(), $this->get_config() );
    }

    private function get_config() {
        return array(
            'id' => 'homebuilder',
            'default_path' => '',
            'menu' => 'homebuilder-plugin-installer',
            'parent_slug' => 'themes.php',
            'capability' => 'edit_theme_options',
            'has_notices' => true,
            'dismissable' => true,
            'dismiss_msg' => '',
            'is_automatic' => false,
            'message' => '',
        );
    }

    private function get_plugins() {
        $imgs = HOMEBUILDER_ASSETS_URI . 'admin/img/';

        return array(

            array(
                'name' => 'Bucket Factory',
                'slug' => 'bucket-factory',
                'source' => HOMEBUILDER_ROOT . 'plugins/bucket-factory.zip',
                'required' => true,
                'version' => '1.0.1',
                'image_url' => esc_url( $imgs . 'bucket_factory.jpg' ),
            ),

            array(
                'name' => 'Contact Form 7',
                'slug' => 'contact-form-7',
                'required' => false,
                'version' => '4.5',
                'image_url' => esc_url( $imgs . 'contact_form_7.jpg' ),
            ),

            array(
                'name' => 'One Click Demo Import',
                'slug' => 'one-click-demo-import',
                'required' => false,
                'version' => '1.2.0',
                'image_url' => esc_url( $imgs . '1click.jpg' ),
            ),

            array(
                'name' => 'Visual Composer',
                'slug' => 'js_composer',
                'required' => false,
                'version' => '4.12',
                'source' => HOMEBUILDER_ROOT . 'plugins/js_composer.zip',
                'image_url' => esc_url( $imgs . 'visual_composer.jpg' ),
            ),

        );
    }

}

new HomeBuilder_Required_Plugins;