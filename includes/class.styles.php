<?php 

class Homebuilder_Styles {

    public function __construct() {
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue') );
    }

    public function enqueue() {
        wp_enqueue_style( 'bootstrap', HOMEBUILDER_VENDOR_ASSETS_URI . 'bootstrap/css/bootstrap.min.css', array(), '3.3.6' );
        //wp_enqueue_style( 'bootstrap-submenu', HOMEBUILDER_VENDOR_ASSETS_URI . 'bootstrap-submenu/css/bootstrap-submenu.min.css', array(), '2.0.4' );
        //wp_enqueue_style( 'fontface-playfair', HOMEBUILDER_ASSETS_URI . 'fonts/fontface-playfair/playfair-webfont.css', array(), HOMEBUILDER_VERSION );
        wp_enqueue_style( 'fontface-open-sans', HOMEBUILDER_ASSETS_URI . 'fonts/fontface-open-sans/open-sans.css', array(), HOMEBUILDER_VERSION );
        //wp_enqueue_style( 'icomoon', HOMEBUILDER_ASSETS_URI . 'fonts/fontface-icomoon/icomoon.css', array(), HOMEBUILDER_VERSION );
        wp_enqueue_style( 'font-awesome', HOMEBUILDER_VENDOR_ASSETS_URI . 'font-awesome/css/font-awesome.min.css', array(), '4.6.1' );
        wp_enqueue_style( 'twentytwenty', HOMEBUILDER_VENDOR_ASSETS_URI . 'twentytwenty/css/twentytwenty.css', array(), HOMEBUILDER_VERSION );
        wp_enqueue_style( 'owl-carousel', HOMEBUILDER_VENDOR_ASSETS_URI . 'owl-carousel/assets/owl.carousel.min.css', array(), '2.1.1' );
        //wp_enqueue_style( 'animate', HOMEBUILDER_VENDOR_ASSETS_URI . 'animate.css/animate.min.css', array(), '3.5.0' );
        //wp_enqueue_style( 'magnific-popup', HOMEBUILDER_VENDOR_ASSETS_URI . 'magnific-popup/magnific-popup.css', array(), '1.1.0' );
        wp_enqueue_style( 'homebuilder-style', HOMEBUILDER_CSS_URI . 'homebuilder.css', array(), HOMEBUILDER_VERSION );
    }

}

new Homebuilder_Styles;