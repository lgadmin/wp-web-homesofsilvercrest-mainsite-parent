/* Core CSS */
#preloader .preloader-container span {
    background-color: {{HOMEBUILDER_COLOR}};
}

#preloader .preloader-container span::before {
    border-color: {{HOMEBUILDER_COLOR}};
}

a:hover,
a:focus {
    color: {{HOMEBUILDER_COLOR}};
}

.form-control:focus {
    border-color: {{HOMEBUILDER_COLOR}};
}

.btn-primary {
    background-color: {{HOMEBUILDER_COLOR}};
    border-color: {{HOMEBUILDER_COLOR}};
}
.btn-primary:focus,
.btn-primary.focus {
    background-color: {{HOMEBUILDER_COLOR}} !important; /* 10% darken */
    border-color: {{HOMEBUILDER_COLOR}} !important; /* 12% darken */
}
.btn-primary:hover {
    background-color: {{HOMEBUILDER_COLOR}}; /* 5% darken */
    border-color: {{HOMEBUILDER_COLOR}}; /* 6% darken */
}

/* Components */

.dropdown-menu > li > a:hover,
.dropdown-menu > li > a:hover:focus,
.dropdown-menu > li > a:hover:active {
    color: {{HOMEBUILDER_COLOR}};
}

.navbar-nav > .active > a,
.navbar-nav > .active > a:hover,
.navbar-nav > .active > a:focus {
    color: {{HOMEBUILDER_COLOR}};
}
.navbar-nav .dropdown-menu .active > a,
.navbar-nav .dropdown-menu:hover > a,
.navbar-nav .dropdown-menu:focus > a {
    color: {{HOMEBUILDER_COLOR}};
}

.nav > li > a:hover,
.nav > li > a:focus {
    color:{{HOMEBUILDER_COLOR}};
}

.nav-pills > li > a:hover,
.nav-pills > li > a:focus {
    color: {{HOMEBUILDER_COLOR}};
}

.nav-pills > li.active > a > .badge,
.nav-pills > li.active > a:hover > .badge,
.nav-pills > li.active > a:focus > .badge {
    color: {{HOMEBUILDER_COLOR}};
}

.breadcrumb > .active {
    color: {{HOMEBUILDER_COLOR}};
}

/* Utility classes */

.icon.icon-primary {
    color: {{HOMEBUILDER_COLOR}};
    border: 1px solid {{HOMEBUILDER_COLOR}};
}

.arrow-primary.arrow:after {
    border-bottom-color: {{HOMEBUILDER_COLOR}};
}

/* Colors */

.bg-primary {
    background-color: {{HOMEBUILDER_COLOR}};
}

.text-primary {
	color: {{HOMEBUILDER_COLOR}};
}

/* Photo grig */

.filter.nav-pills > li > a:focus,
.filter.nav-pills > li > a:hover {
    background: {{HOMEBUILDER_COLOR}};
}

/* Third party */

.twentytwenty-handle {
    background-color: <?php echo esc_attr(homebuilder_hex2rgba($color, 0.8)); ?>;
}

.owl-dots .owl-dot.active span {
    background-color: {{HOMEBUILDER_COLOR}};
}
.owl-dots .owl-dot.active span::before {
    border: 1px solid {{HOMEBUILDER_COLOR}};
}

#owlNews .news_item:hover .news_details {
    background-color: {{HOMEBUILDER_COLOR}};
}
#owlNews .news_item:hover .news_details .arrow::after {
    border-bottom-color: {{HOMEBUILDER_COLOR}};
}
#owlClients .owl-nav .owl-prev:hover,
#owlClients .owl-nav .owl-next:hover {
    background-color: {{HOMEBUILDER_COLOR}};
}

.owl-carousel.dots-inner .owl-dots {
    background-color: {{HOMEBUILDER_COLOR}};
}
.owl-carousel.dots-center .owl-nav .owl-prev,
.owl-carousel.dots-center .owl-nav .owl-next {
    background-color: {{HOMEBUILDER_COLOR}};
}
.owl-carousel.owlGallery .owl-nav .owl-prev,
.owl-carousel.owlGallery .owl-nav .owl-next {
    background-color: {{HOMEBUILDER_COLOR}};
}

/* Brand */

.go-top.active:hover {
    background-color: {{HOMEBUILDER_COLOR}};
}

.section_heading .title::before {
    background-color: {{HOMEBUILDER_COLOR}};
}

.featured-box:hover .icon {
   background-color: {{HOMEBUILDER_COLOR}} ;
   border-color: {{HOMEBUILDER_COLOR}} ;
}

.featured-box:hover .icon::before {
   border-color: {{HOMEBUILDER_COLOR}} ;
}

.featured-box-pendulum .icon {
   color: {{HOMEBUILDER_COLOR}} ;
   border-color: {{HOMEBUILDER_COLOR}} ;
}

.featured-box-pendulum .icon-pendulum {
   background-color: {{HOMEBUILDER_COLOR}} ;
}

.featured-box-pendulum .icon-pendulum-circle {
   border-color: {{HOMEBUILDER_COLOR}} ;
}

.recent_project .recent_project_details .details_wrapper {
     background-color: <?php echo esc_attr(homebuilder_hex2rgba($color, 0.8)); ?>;
}

.panel_accordion .panel-default .panel-heading .panel-title a[aria-expanded="true"],
.panel_accordion .panel-default .panel-heading .panel-title a:hover,
.panel_accordion .panel-default .panel-heading .panel-title a:active {
    background-color: {{HOMEBUILDER_COLOR}};
    border: 1px solid {{HOMEBUILDER_COLOR}};
}

.nav_services > li.active > a {
    color: {{HOMEBUILDER_COLOR}};
    border-bottom-color: {{HOMEBUILDER_COLOR}};
}
.nav_services > li.active::after {
    background-color: {{HOMEBUILDER_COLOR}};
}

.pagination > li > a:hover,
.pagination > li > a:focus {
    border-color: {{HOMEBUILDER_COLOR}};
    background-color: {{HOMEBUILDER_COLOR}};
}
.pagination > li.active > a,
.pagination > li.active > a:hover,
.pagination > li.active > a:focus {
    border-color: {{HOMEBUILDER_COLOR}};
    background-color: {{HOMEBUILDER_COLOR}};
}

.pager > li > a:hover,
.pager > li > a:focus,
.pager > li > a:active {
    border-color: {{HOMEBUILDER_COLOR}};
    background-color: {{HOMEBUILDER_COLOR}};
}

.comment-respond .submit {
    background-color: {{HOMEBUILDER_COLOR}};
    border-color: {{HOMEBUILDER_COLOR}};
}
.comment-respond .submit:focus,
.comment-respond .submit.focus {
    background-color:{{HOMEBUILDER_COLOR}} !important; /* 10% darken */
    border-color: {{HOMEBUILDER_COLOR}} !important; /* 12% darken */
}
.comment-respond .submit:hover {
    background-color:{{HOMEBUILDER_COLOR}}; /* 5% darken */
    border-color: {{HOMEBUILDER_COLOR}}; /* 6% darken */
}

.widget-title::before {
    background-color: {{HOMEBUILDER_COLOR}};
}

.current-menu-item > a {
  color: {{HOMEBUILDER_COLOR}}; 
}

.nav .current-menu-parent > a {
    color: {{HOMEBUILDER_COLOR}}; 
}

.b-primary{
    border-color: {{HOMEBUILDER_COLOR}};
}

.text-primary{
    color: {{HOMEBUILDER_COLOR}}
}