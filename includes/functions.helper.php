<?php 

/**
 * Retrive data from theme options
 * @param  int  $key     
 * @param  int  $default 
 * @return void
 */
function homebuilder_get_option($key, $default = false) {
    $data = get_option( CS_OPTION );
    return ( isset($data[$key]) ? $data[$key] : $default );
}

/**
 * Print homebuilder breadcrumb
 * @return string
 */
function homebuilder_breadcrumbs() {
    if ( function_exists( 'breadcrumb_trail') ) {
        $args = array(
            'show_browse' => false,
            );
        echo breadcrumb_trail( $args );
    }
}

/**
 * Print homebuilder all service list
 * @param $service_id
 * @return string
 */
function homebuilder_all_services( $service_id ){
    $args = array(
        'post_type' => 'service',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        );
    $all_servie_query = new WP_Query( $args );

    if ( $all_servie_query->have_posts() ) {
        while ($all_servie_query->have_posts()) {
            $all_servie_query->the_post();
            $id = get_the_ID();
            $active_class = ($id === $service_id ? 'active' : '');
            printf('<li class="%1$s"><a href="%2$s">%3$s</a></li>',
                $active_class,
                esc_url( get_the_permalink() ),
                esc_html( get_the_title() )
            );
        }
    }
    wp_reset_postdata();
}


/**
 * Get homebuilder brand logo
 * @return string
 */
function homebuilder_get_logo( $logo_id = 0, $retina_logo_id = 0 ){
    $default    = wp_get_attachment_image_src( $logo_id, 'full' );
    $retina     = wp_get_attachment_image_src( $retina_logo_id, 'full' );
    $retina_url = '';
    if ( isset( $retina[0] ) ) {
        $retina_url = esc_url( $retina[0] );
    }
    if ( isset( $default[0] ) ) {
        return sprintf( '<a class="navbar-brand logo" href="%1$s"><img src="%2$s" data-retina="%3$s" alt="%4$s"></a>',
            esc_url( home_url( '/' ) ),
            esc_url( $default[0] ),
            $retina_url,
            esc_attr( get_bloginfo( 'name' ) )
        );
    } else{
        return sprintf( '<a class="navbar-brand h4" href="%1$s">%2$s</a>',
            esc_url( home_url( '/' ) ),
            esc_attr( get_bloginfo( 'name' ) )
        );
    }
}

/**
 * Get homebuilder primary nav
 * @return string
 */
function homebuilder_get_primary_nav(){
    wp_nav_menu( array(
        'menu'              => 'primary',
        'theme_location'    => 'primary',
        'depth'             => 0,
        'container'         => 'div',
        'container_class'   => 'collapse navbar-collapse mainNav',
        'container_id'      => '',
        'menu_class'        => 'nav navbar-nav pull-right text-u-c',
        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
        'walker'            => new wp_bootstrap_navwalker() )
    ); 
}


/**
 * Generate column based on sidebar setting
 * @param  string $sidebar
 * @return array
 */
function homebuilder_get_column_class( $sidebar = 'right' ) {
    if ( 'right' == $sidebar ) {
        $column = array( 'main' => 'col-md-8', 'sidebar' => 'col-md-4' );
    } elseif ( 'left' == $sidebar ) {
        $column = array( 'main' => 'col-md-8 col-md-push-4', 'sidebar' => 'col-md-4 col-md-pull-8' );
    } elseif ( 'no-sidebar' == $sidebar ) {
        $column = array( 'main' => 'col-md-12', 'sidebar' => '' );
    } else{
        $column = array( 'main' => 'col-md-12', 'sidebar' => '' );
    }
    return $column;
}


function homebuilder_posts_navigation() {
    global $wp_query;
    $big = 999999999; // need an unlikely integer

    $links = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'type' => 'array',
        'prev_next' => true,
        'prev_text' => esc_html__('Prev', 'homebuilder'),
        "next_text" => esc_html__('Next', 'homebuilder'),
        'mid_size' => 3
        ));
        ?>
        <ul class="pagination pagination-lg">
            <?php
            if ($links) {
                foreach ($links as $link) {
                    if (strpos($link, "current") !== false)
                        echo '<li class="active page-numbers"><a>' . $link . "</a></li>\n";
                    else
                        echo '<li class="page-numbers">' . $link . "</li>\n";
                }
            }
            ?>
        </ul>
        <?php
    }

/**
 * Display author link
 * @return string
 */
function homebuilder_author_link() {
    $output = '<span>' . esc_html__( 'Posted by ', 'homebuilder' );
    $output .= '<a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a>';
    $output .= '</span> / ';
    return $output;
}

/**
 * Display post categories
 * @param  array  $categories
 * @return string
 */
function homebuilder_get_post_categories( $categories ){
    $output = '';
    $separator = ', ';
    foreach( $categories as $category ) {
        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . sprintf( esc_attr__( 'View all posts in %s', 'homebuilder' ), $category->name ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
    }
    echo '<span>' . trim( $output, $separator ) . '</span> / ' ;
}


/**
 * Display comment number
 * @return string
 */
function homebuilder_post_comments_number() {
    $num_comments = get_comments_number();
    $write_comments = '';
    if ( comments_open() ) {
        if ( $num_comments == 0 ) {
            $comments = esc_html__( 'No Comments', 'homebuilder' );
        } elseif ( $num_comments > 1 ) {
            $comments = $num_comments . ' ' . esc_html__( 'Comments', 'homebuilder' );
        } else {
            $comments = esc_html__( '1 Comment', 'homebuilder' );
        }
        $write_comments = '<a href="' . esc_url( get_comments_link() ) .'">'. $comments.'</a>';
    } else {
        $write_comments =  esc_html__( 'Comments are off for this post.', 'homebuilder' );
    }

    echo '<span>' . $write_comments . '</span>';
}

/**
 * Get previous post link
 * @return string
 */
function homebuilder_get_previous_post_link() {
    $previous_post = get_previous_post();
    $link          = '';
    if ( ! empty( $previous_post ) ) {
        $link = get_the_permalink( $previous_post->ID );
    }
    return $link;
}

/**
 * Get next post link
 * @return srting
 */
function homebuilder_get_next_post_link() {
    $next_post = get_next_post();
    $link      = '';
    if ( ! empty( $next_post ) ) {
        $link = get_the_permalink( $next_post->ID );
    }
    return $link;
}

/**
 * Get previous post title
 * @return string
 */
function homebuilder_get_previous_post_title() {
    $previous_post = get_previous_post();
    $title         = '';
    if ( ! empty( $previous_post ) ) {
        $title = get_the_title( $previous_post->ID );
    }
    return $title;
}

/**
 * Get next post title
 * @return string
 */
function homebuilder_get_next_post_title() {
    $next_post = get_next_post();
    $title     = '';
    if ( ! empty( $next_post ) ) {
        $title = get_the_title( $next_post->ID );
    }
    return $title;
}

/**
 * Check single post pagination status 
 * @return string
 */
function homebuilder_check_link_status( $title ){
    $class = '';
    if ( empty( $title ) ) {
        $class ='disabled';
    } 
    return $class;
}

/**
 * Change HEX to RGBA color
 */
function homebuilder_hex2rgba($color, $opacity = false) {

    $default = 'rgb(0,0,0)';

    //Return default if no color provided
    if(empty($color))
        return $default;

    //Sanitize $color if "#" is provided
    if ($color[0] == '#' ) {
        $color = substr( $color, 1 );
    }

    //Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
        return $default;
    }

    //Convert hexadec to rgb
    $rgb =  array_map('hexdec', $hex);

    //Check if opacity is set(rgba or rgb)
    if($opacity){
        if(abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
    } else {
        $output = 'rgb('.implode(",",$rgb).')';
    }

    //Return rgb(a) color string
    return $output;
}

function homebuilder_sanitize_param( $param ) {
    return strtolower( trim( $param ) );
}

function homebuilder_sanitize_css_unit( $value, $default ) {
    $units = '/-?\d+[px|em|%|pt|cm|ex|mm|in|rem]/';
    if ( preg_match( $units, $value ) ) {
        return $value;
    } else {
        return $value . $default;
    }
}

function homebuilder_check_css_unit( $value, $default = 'px' ) {
    $value  = homebuilder_sanitize_param( $value );
    
    if ( $value === 0 || $value === '0' || $value === '' )
        $value = '0px';

    $values = array_filter( explode( ' ', $value ) );
    $out = array();
    foreach ( $values as $val ) {
        $out[] = homebuilder_sanitize_css_unit( $val, $default );
    }
    return implode( ' ', $out );
}