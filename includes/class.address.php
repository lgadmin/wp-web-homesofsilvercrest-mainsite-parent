<?php
/**
 * Address widget.
 *
 * @package HomeBuilder
 * @author ThemeBucket <themebucket@gmail.com>
 */

class Homebuilder_Address extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'homebuilder-address',
            esc_html__( 'Homebuilder Address', 'homebuilder' ),
            array(
                'description' => esc_html__( 'Homebuilder address widget.', 'homebuilder' )
            )
        );
    }


    public function widget($args, $instance) {
        $allowed_html = array(
            'a' => array(
                'href' => array(),
                'title' => array()
            ),
            'br'     => array(),
            'strong' => array(),
            'p'      => array(),
            'span'   => array(),
        );
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );
        $address = isset( $instance['address'] ) ? $instance['address'] : '' ;
        $phone = isset( $instance['phone'] ) ? $instance['phone'] : '' ;
        $email = isset( $instance['email'] ) ? $instance['email'] : '' ;

        echo $before_widget;

        if ( $title ) echo $before_title . $title . $after_title; ?>

        <div class="textwidget">                
            <div class="m-t-lg">
                <?php if ( !empty( $address ) ) { ?>
                <div class="row">
                    <div class="col-xs-3"><?php esc_html_e( 'Address', 'homebuilder' ) ?></div>
                    <div class="col-xs-1">:</div>
                    <div class="col-xs-8"><?php echo wp_kses( $address, $allowed_html ) ?></div>
                </div>
                <?php } if ( !empty( $phone ) ) { ?>
                <div class="line m-v b-b b-a"></div>
                <div class="row">
                    <div class="col-xs-3"><?php esc_html_e( 'Phone', 'homebuilder' ); ?></div>
                    <div class="col-xs-1">:</div>
                    <div class="col-xs-8"><?php echo wp_kses( $phone, $allowed_html ) ?></div>
                </div>
                <?php } if ( !empty( $email ) ) { ?>
                <div class="line m-v b-b b-a"></div>
                <div class="row">
                    <div class="col-xs-3"><?php esc_html_e( 'Email', 'homebuilder' ); ?></div>
                    <div class="col-xs-1">:</div>
                    <div class="col-xs-8"><?php echo wp_kses( $email, $allowed_html ) ?></div>
                </div>
                <?php } ?>
            </div>
        </div>

        <?php echo $after_widget;
    }

    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title']   = strip_tags($new_instance['title']);
        $instance['phone']   = $new_instance['phone'];
        $instance['email']   = $new_instance['email'];
        $instance['address'] = $new_instance['address'];
        return $instance;
    }

    public function form( $instance ) {
        if ( $instance ) {
            $title   = $instance['title'];
            $phone   = isset( $instance['phone'] ) ? $instance['phone'] : '';
            $email   = isset( $instance['email'] ) ? $instance['email'] : '';
            $address = isset( $instance['address'] ) ? $instance['address'] : '';
        } else{
            $title   = '';
            $address = '';
            $phone   = '';
            $email   = '';
        }
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php esc_html_e('Title', 'homebuilder'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('address'); ?>"><?php esc_html_e('Address:', 'homebuilder'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" rows="7" cols="20" ><?php echo esc_textarea( $address ); ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('phone'); ?>"><?php esc_html_e('Phone:', 'homebuilder'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" rows="3" cols="20" ><?php echo esc_textarea( $phone ); ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('email'); ?>"><?php esc_html_e('Email:', 'homebuilder'); ?></label>
            <textarea class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" rows="3" cols="20" ><?php echo esc_textarea( $email ); ?></textarea>
        </p>
        <?php
    }
}
add_action( 'widgets_init', create_function( '', 'register_widget("Homebuilder_Address");' ) );
