<?php 

class Homebuilder_Scripts {

    public function __construct() {
        add_action( 'wp_head', array($this, 'enqueue_ie_only') );
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue') );
        add_action('wp_default_scripts', array($this, 'remove_jquery_migrate'));
        add_action( 'wp_footer', array($this, 'my_deregister_scripts') );

    }

    public function enqueue() {

        /** Vendor scripts */
        wp_enqueue_script( 'bootstrap', HOMEBUILDER_VENDOR_ASSETS_URI . 'bootstrap/js/bootstrap.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'bootstrap-submenu', HOMEBUILDER_VENDOR_ASSETS_URI . 'bootstrap-submenu/js/bootstrap-submenu.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'bootstrap-hover-dropdown', HOMEBUILDER_VENDOR_ASSETS_URI . 'bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'event-move', HOMEBUILDER_VENDOR_ASSETS_URI . 'twentytwenty/js/jquery.event.move.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'twentytwenty', HOMEBUILDER_VENDOR_ASSETS_URI . 'twentytwenty/js/jquery.twentytwenty.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'owl-carousel', HOMEBUILDER_VENDOR_ASSETS_URI . 'owl-carousel/owl.carousel.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'stickyfill', HOMEBUILDER_VENDOR_ASSETS_URI . 'stickyfill/stickyfill.min.js', array(), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'headroom', HOMEBUILDER_VENDOR_ASSETS_URI . 'headroom/headroom.min.js', array(), HOMEBUILDER_VERSION, true );
        //wp_enqueue_script( 'masonry', HOMEBUILDER_VENDOR_ASSETS_URI . 'masonry/masonry.pkgd.min.js', array(), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'imageloaded', HOMEBUILDER_VENDOR_ASSETS_URI . 'imagesloaded/imagesloaded.min.js', array(), HOMEBUILDER_VERSION, true );
        //wp_enqueue_script( 'isotope', HOMEBUILDER_VENDOR_ASSETS_URI . 'isotope/isotope.pkgd.min.js', array(), HOMEBUILDER_VERSION, true );
        //wp_enqueue_script( 'magnific-popup', HOMEBUILDER_VENDOR_ASSETS_URI . 'magnific-popup/jquery.magnific-popup.min.js', array(), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'waypoints', HOMEBUILDER_VENDOR_ASSETS_URI . 'waypoints/jquery.waypoints.min.js', array(), HOMEBUILDER_VERSION, true );
        //wp_enqueue_script( 'wow', HOMEBUILDER_VENDOR_ASSETS_URI . 'wow/wow.min.js', array(), HOMEBUILDER_VERSION, true );
        wp_enqueue_script( 'countTo', HOMEBUILDER_VENDOR_ASSETS_URI . 'countTo/jquery.countTo.min.js', array('jquery'), HOMEBUILDER_VERSION, true );
        //wp_enqueue_script( 'appear', HOMEBUILDER_JS_URI . 'jquery.appear.min.js', array('jquery'), HOMEBUILDER_VERSION, true );

        /** Homebuilder script */
        wp_enqueue_script( 'homebuilder-scripts', HOMEBUILDER_JS_URI . 'homebuilder.js', array('jquery'), HOMEBUILDER_VERSION, true );

        /** Default scripts */
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }

    public function enqueue_ie_only() {
        ?>
        <!--[if lt IE 9]>
        <script src="<?php echo esc_url( HOMEBUILDER_JS_URI . 'html5shiv.js' ); ?>"></script>
        <script src="<?php echo esc_url( HOMEBUILDER_JS_URI . 'respond.min.js' ); ?>"></script>
        <![endif]-->
        <?php
    }

    /*Remove WP embed*/
    public function my_deregister_scripts(){
        wp_deregister_script( 'wp-embed' );
    }

    /*Remove jquery migrate js*/
    public function remove_jquery_migrate($scripts)
    {
        if (!is_admin() && isset($scripts->registered['jquery'])) {
            $script = $scripts->registered['jquery'];

            if ($script->deps) { // Check whether the script has any dependencies
                $script->deps = array_diff($script->deps, array(
                    'jquery-migrate'
                ));
            }
        }
    }
}

new Homebuilder_Scripts;