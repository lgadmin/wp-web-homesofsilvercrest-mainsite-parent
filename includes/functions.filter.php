<?php
/*
 * Append read more button to post excerpt
 * @param  string $excerpt
 * @return string
 */
function homebuilder_append_readmore( $excerpt ) {
    $readmore_text = homebuilder_get_option( 'read_more_text', esc_html__( 'Read more', 'homebuilder' ) );
    $readmore = sprintf( '<p class="m-t-xl"><a href="%s" title="%s" class="m-t-xl">%s <i class="fa fa-long-arrow-right"></i></a></p>',
        esc_url( get_the_permalink() ),
        sprintf( esc_attr__( 'Permalink to: %s', 'homebuilder' ), get_the_title() ),
        $readmore_text
        );

    return $excerpt . $readmore;
}
add_filter( 'the_excerpt', 'homebuilder_append_readmore' );


/**
 * Display post excerpt length
 * @param  string $excerpt
 * @return integer
 */
function homebuilder_post_excerpt_length( $length ){
    $length = absint( homebuilder_get_option( 'excerpt_length', 50 ) );
    return $length;
}
add_action( 'excerpt_length', 'homebuilder_post_excerpt_length' );

/**
 * Clean excerpt more
 */
function homebuilder_clean_excerpt_more( $more ) {
   return ' ...';
}
add_filter( 'excerpt_more', 'homebuilder_clean_excerpt_more' );

/**
 * Update default gallery
 * @return integer
 */
function homebuilder_update_gallery_style( ) { 
    // set owl-carousel as default gallery
    return '<div id="owlSingleImage" class="owl-carousel dots-inner">';
};          
add_filter( 'gallery_style', 'homebuilder_update_gallery_style', 10, 1 );


function homebuilder_backtotop() {
    $has_backtotop = homebuilder_get_option( 'display_backtotop', true );
    if ( $has_backtotop ) { ?>
        <div class="go-top"><i class="fa fa-chevron-up"></i></div>
    <?php
    }
}
add_action( 'wp_footer','homebuilder_backtotop' );

// add user defined styles
function homebuilder_enqueue_user_css() {
    $data = cs_get_option( 'homebuilder_css' );
    $data = trim( preg_replace( array('/<style[^>]*>/i','/<\/style>/i'), '', $data ) );
    if ( $data )
        echo sprintf('<style id="Homebuilder-user-styles">%s</style>', $data );
}
add_action( 'wp_head', 'homebuilder_enqueue_user_css', 999999999 );

// add user defined scripts
function homebuilder_enqueue_user_js() {
    $data = cs_get_option( 'homebuilder_js' );
    $data = trim( preg_replace( array('/<script[^>]*>/i','/<\/script>/i'), '', $data ) );
    if ( $data )
        echo sprintf('<script id="Homebuilder-user-scripts">%s</script>', $data );
}
add_action( 'wp_footer', 'homebuilder_enqueue_user_js', 999999999 );


/**
 * Engueue user defined brand color
 */
function homebuilder_enqueue_brand_color( $css ) {
    $color = cs_get_option('brand_color');

    if ( cs_get_option('custom_brand_color') && $color !== '#d6b161' ) {
        ob_start();
        include HOMEBUILDER_INCLUDES_DIR . 'brand-color.php';
        $contents = ob_get_clean();
        $css .= str_replace( '{{HOMEBUILDER_COLOR}}', esc_attr($color), $contents );
    }

    printf( "<style id='homebuilder-brand-color' type='text/css'>\n%s\n</style>",
        $css
    );
}
add_filter( 'wp_head', 'homebuilder_enqueue_brand_color' );

function homebuilder_add_additional_mce_buttons( $buttons ) {
    $buttons[] = 'fontselect';
    $buttons[] = 'fontsizeselect';
    $buttons[] = 'backcolor';
    return $buttons;
}
add_filter( 'mce_buttons_3', 'homebuilder_add_additional_mce_buttons' );

function homebuilder_add_fontsize_formats( $config ){
    $config['fontsize_formats'] = "9px 10px 12px 13px 14px 16px 18px 21px 24px 28px 32px 36px 40px 44px 48px 54px 60px 66px 72px 80px 88px 96px";
    return $config;
}
add_filter( 'tiny_mce_before_init', 'homebuilder_add_fontsize_formats' );