<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package homebuilder
 */

if ( ! function_exists( 'homebuilder_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function homebuilder_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( 'Posted on %s', 'post date', 'homebuilder' ),
		'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
	);

	$byline = sprintf(
		esc_html_x( 'by %s', 'post author', 'homebuilder' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.

}
endif;

if ( ! function_exists( 'homebuilder_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function homebuilder_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'homebuilder' ) );
		if ( $categories_list && homebuilder_categorized_blog() ) {
			printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'homebuilder' ) . '</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'homebuilder' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'homebuilder' ) . '</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'homebuilder' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
		echo '</span>';
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'homebuilder' ),
			the_title( '<span class="screen-reader-text">"', '"</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function homebuilder_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'homebuilder_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'homebuilder_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so homebuilder_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so homebuilder_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in homebuilder_categorized_blog.
 */
function homebuilder_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'homebuilder_categories' );
}
add_action( 'edit_category', 'homebuilder_category_transient_flusher' );
add_action( 'save_post',     'homebuilder_category_transient_flusher' );


function homebuilder_social_media() {
    $icons = cs_get_option( 'social_media', array() );

    if ( empty( $icons ) ) {
        return;
    }

    foreach( $icons as $icon ) {
        $name      = isset( $icon['name'] ) ? $icon['name'] : '';
        $link      = isset( $icon['link'] ) ? $icon['link'] : '';
        $link      = ( 'email' == $name ) ? 'mailto:' . esc_attr( $link ) : esc_url( $link );
        $icon_name = '';

        if ( 'email' == $name ) {
            $icon_name = 'fa fa-envelope-o';
        } elseif ( 'custom' == $name ) {
            $icon_name = isset( $icon['icon-name'] ) ? $icon['icon-name'] : '';
        } else {
            $icon_name = "fa fa-{$name}";
        }

        printf( '<a href="%s" class="m-r-sm" target="_blank" rel="noopener"><i class="%s"></i></a>',
            $link,
            esc_attr( $icon_name )
            );
    }
}

function homebuilder_topbar_right( $phone, $email, $time ){
    
    if( !empty( $phone) ){
        echo '<a href="tel:'. esc_attr( $phone ) .'" class="text-sm m-h-sm"><i class="fa fa-phone m-r-xs"></i> ' . esc_html( $phone ) . ' </a>';
    } 
    if ( !empty( $email ) ) {
        echo '<a href="mailto:'. esc_attr( $email ) .'" class="text-sm m-h-sm"><i class="fa fa-envelope-o m-r-xs"></i> ' . esc_html( $email ) . '</a>';
    }
    if ( !empty( $time ) ) {
        echo '<span class="text-sm m-h-sm"><i class="fa fa-clock-o m-r-xs"></i> ' . esc_html( $time ) . '</span>';
    } 
}

function homebuilder_preloader(){ 
    echo '<div id="preloader"><div class="preloader-container"><span></span><span></span><span></span></div></div>';
}


/**
 * Get project categorires to display on project shortcode
 * @param $categories
 * @return string
 */
function homebuilder_get_project_categories( $categories ){
    $output = ', ';
    $separator = ', ';
    foreach( $categories as $category ) {
        $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" title="' . esc_attr( sprintf( __( 'View all posts in %s', 'homebuilder' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
    }
    return trim( $output, $separator );
}