<?php 


class Homebuilder_Sidebars {

    public function __construct() {
        add_action( 'widgets_init', array($this, 'register') );
    }

    public function register() {
        register_sidebar( array(
	        'name'          => esc_html__( 'Sidebar', 'homebuilder' ),
	        'id'            => 'sidebar-1',
	        'description'   => esc_html__( 'Add widgets here.', 'homebuilder' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h2 class="widget-title">',
	        'after_title'   => '</h2>',
	    ) );

        register_sidebar( array(
            'name'          => esc_html__( 'Footer First Column', 'homebuilder' ),
            'id'            => 'footer-1',
            'description'   => esc_html__( 'Add widgets here for footer first column.', 'homebuilder' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<div class="section_heading"><h5 class="title title_sm">',
            'after_title'   => '</h5></div>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__( 'Footer Second Column', 'homebuilder' ),
            'id'            => 'footer-2',
            'description'   => esc_html__( 'Add widgets here for footer second column.', 'homebuilder' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<div class="section_heading"><h5 class="title title_sm">',
            'after_title'   => '</h5></div>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__( 'Footer Third Column', 'homebuilder' ),
            'id'            => 'footer-3',
            'description'   => esc_html__( 'Add widgets here for footer third column.', 'homebuilder' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<div class="section_heading"><h5 class="title title_sm">',
            'after_title'   => '</h5></div>',
        ) );

        register_sidebar( array(
            'name'          => esc_html__( 'Footer Fourth Column', 'homebuilder' ),
            'id'            => 'footer-4',
            'description'   => esc_html__( 'Add widgets here for footer fourth column.', 'homebuilder' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<div class="section_heading"><h5 class="title title_sm">',
            'after_title'   => '</h5></div>',
        ) );

    }

}

new Homebuilder_Sidebars;