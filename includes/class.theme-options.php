<?php

class Homebuilder_Theme_Options {

    public function __construct() {
        add_action( 'init', array($this, 'setup_options_panel') );
        add_filter( 'cs_framework_options', array($this, 'register_options') );
    }

    public function setup_options_panel() {
        $settings           = array(
              'menu_title'      => esc_html__( 'Theme Options', 'homebuilder' ),
              'menu_type'       => 'theme',
              'menu_slug'       => 'theme-options',
              'ajax_save'       => true,
              'show_reset_all'  => false,
              'framework_title' => esc_html( 'HomeBuilder Options', 'homebuilder' ),
            );

        $options  = array();
        CSFramework::instance( $settings, $options );
    }

    public function register_options( $homebuilder_options ) {
        $dir = trailingslashit( HOMEBUILDER_INCLUDES_DIR . 'options' );
        $imgs = trailingslashit( HOMEBUILDER_ASSETS_URI . 'admin/img' );

        $sections = array(
            'general',
            'header',
            'page',
            'service',
            'project',
            'blog',
            'social',
            'typography',
            'footer',
            'custom',
            'backup'
            );

        foreach ( $sections as $section ) {
            $section_file = "{$dir}{$section}.php";
            if ( file_exists( $section_file ) )
                include_once $section_file;
        }

        return $homebuilder_options;
    }

}

new Homebuilder_Theme_Options;

