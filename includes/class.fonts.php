<?php
/**
 * Enqueue user selected google fonts.
 *
 * @package HomeBuilder
 * @author ThemeBucket <themebucket@gmail.com>
 */

class Homebuilder_Fonts {

    public function __construct() {
        add_action( 'wp_enqueue_scripts', array($this, 'enqueue') );
    }

    public function enqueue() {
        if ( $user_font = $this->get_user_fonts() ) {
            wp_enqueue_style( 'homebuilder-user-font', $user_font, array(), HOMEBUILDER_VERSION );
        }
    }

    protected function get_user_fonts() {
        $font_url = '';
        $google_font_stack = homebuilder_get_google_font_stack();

        if ( cs_get_option( 'enable_typography' ) && ! empty( $google_font_stack ) ) {
            $font_url = add_query_arg( 'family', urlencode( $google_font_stack ), '//fonts.googleapis.com/css' );
        }

        return $font_url;
    }

}

new Homebuilder_Fonts();
