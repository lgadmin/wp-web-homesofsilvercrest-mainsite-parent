<?php
/**
 * Static image widget.
 *
 * @package HomeBuilder
 * @author ThemeBucket <themebucket@gmail.com>
 */

class Homebuilder_Social extends WP_Widget {

    public function __construct() {
        parent::__construct(
            'homebuilder-social',
            esc_html__( 'Homebuilder Social', 'homebuilder' ),
            array(
                'description' => esc_html__( 'Social media widget.', 'homebuilder' )
            )
        );
    }

    public function widget($args, $instance) {
        extract( $args );
        $title = apply_filters( 'widget_title', $instance['title'] );

        echo $before_widget;

        if ( $title ) echo $before_title . $title . $after_title;

        homebuilder_social_media();
        
        echo $after_widget;
    }

    public function update($new_instance, $old_instance) {
    }

    public function form($instance) {
    }

}
add_action( 'widgets_init', create_function( '', 'register_widget("Homebuilder_Social");' ) );
