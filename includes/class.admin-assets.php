<?php
/**
 * This class handles all the admin related assets.
 *
 * @package Homebuilder
 * @author ThemeBucket <themebucket@gmail.com>
 */

class Homebuilder_Admin_Assets {

    public function __construct() {
        add_action( 'admin_enqueue_scripts', array($this, 'enqueue_scripts'), 20 );
    }

    function enqueue_scripts( $hook ) {
        wp_enqueue_script( 'homebuilder-admin', HOMEBUILDER_ASSETS_URI . 'admin/js/homebuilder.js', array('jquery'), HOMEBUILDER_VERSION, true );
        wp_enqueue_style( 'icomoon', HOMEBUILDER_ASSETS_URI . 'fonts/fontface-icomoon/icomoon.css', array(), HOMEBUILDER_VERSION );
        wp_enqueue_style( 'homebuilder-admin', HOMEBUILDER_ASSETS_URI . 'admin/css/homebuilder.css', array(), HOMEBUILDER_VERSION );
    }

}

new Homebuilder_Admin_Assets;