/**
 * Home Builder v1.0.0 (http://themebucket.net/)
 * Copyright 2015-2016 ThemeBucket
 * Licensed under the Envato license
 */
;(function($) {

    'use strict';

    var $window = $(window);
    var windowWidth = $window.width();

    $(function() {

        //init wow animation
        if (typeof WOW === 'function') {
            new WOW().init();
        }

        // Preloader
        $(window).load(function(){
            $('#preloader').fadeOut(500);
        });

        /* ---------------------------------------------
         Retina init
         --------------------------------------------- */
        if (window.devicePixelRatio > 1) {
            $('[data-retina]').imagesLoaded(function () {
                $('[data-retina]').each(function () {
                    var _img = $(this),
                        retinaSrc = _img.data('retina'),
                        height = _img.height();

                    if ( retinaSrc ) {
                        _img
                            .attr("src", retinaSrc)
                            .css({
                                height: height+'px',
                                width: 'auto'
                            });
                    }
                });
            });
        }

        // Remove extra margin from cover slider wrapper
        $('.owlCoverSlider').parents('.vc_column-inner').addClass('wrapper-none')

        //owl carousel [banner]
        $('.owlCoverSlider').each(function() {
            var banner = $(this);
            banner.owlCarousel({
                loop: true,
                items: 1,
                nav: true,
                margin: 0,
                mouseDrag: false,
                autoplay: Boolean(banner.data('autoplay')),
                autoplayTimeout: Number(banner.data('speed')),
                navText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
            });
        });

        // init Isotope
        if (typeof isotope === 'function') {
            var $grid = $('.photo_grid').isotope({
                itemSelector: '.photo_container'
            });
        }


        // bind filter button click
        $('.filter').on( 'click', 'a', function(e) {
            e.preventDefault();
            var filterValue = $(this).attr('data-filter');
            $(this).parent().addClass('active').siblings().removeClass('active');
            $grid.isotope({ filter: filterValue });
        });


        // go-top
        $window.scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.go-top').addClass('active');
            } else {
                $('.go-top').removeClass('active');
            }
        });
        $('.go-top').click(function() {
            $('html,body').animate({
                scrollTop : 0
            }, 500);
            return false;
        });


        // bootstrap-submenu initialize
        $('[data-submenu]').submenupicker();

        // twentytwenty before after
        $window.load(function(){
            $(".twentytwenty-container").twentytwenty();
        });

        // masonry grid
        if (typeof masonry === 'function' && typeof imagesLoaded === 'function') {
            var $gridMasonry = $('.grid_masonry').masonry({
                itemSelector: '.photo_container',
                gutter: 0
            });
            // layout Masonry after each image loads
            $gridMasonry.imagesLoaded().progress(function () {
                $gridMasonry.masonry('layout');
            });

            // normal grid
            var $gridNormal = $('.grid_normal').masonry({
                itemSelector: '.photo_container',
                gutter: 0
            });
            $gridNormal.imagesLoaded().progress(function () {
                $gridNormal.masonry('layout');
            });
        }

        if(typeof magnificPopup === 'function'){
            $('.magnificGallery').magnificPopup({
                delegate: '.photo_container .photo_thumbnail', // child items selector, by clicking on it popup will open
                type: 'image',
                gallery: {
                    enabled: true
                },
                image: {
                    titleSrc: 'title'
                }
            });
        }


        // owl carousel 
        // [news]
        $('#owlNews').each(function() {
            var news = $(this);
            news.owlCarousel({
                margin: 30,
                loop: true,
                autoplay: news.data('autoplay'),
                autoplayTimeout: news.data('speed'),
                responsive : {
                    1200 : {
                        items: 3
                    },
                    768 : {
                        items: 2
                    },
                    640 : {
                        items: 2
                    },
                    480 : {
                        items: 1
                    },
                    320 : {
                        items: 1
                    }
                }
            });
        });

        //Carosuel - one
        $('#owlClients').each(function() {
            var clients = $(this);
            clients.owlCarousel({
                autoplay: clients.data('autoplay'),
                autoplayTimeout: clients.data('speed'),
                loop: true,
                items: 6,
                margin: 30,
                dots: false,
                nav: true,
                navText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive : {
                    1200 : {
                        items: 6
                    },
                    768 : {
                        items: 5
                    },
                    640 : {
                        items: 4
                    },
                    480 : {
                        items: 3
                    },
                    320 : {
                        items: 2
                    }
                }
            });
        });

        //Carosuel - twoowlCoverSlider
        $('.owlGallery').each(function() {
            var carousel = $(this);
            carousel.owlCarousel({
                autoplay: carousel.data('autoplay'),
                autoplayTimeout: carousel.data('speed'),
                loop: true,
                margin: 60,
                nav: true,
                dots: false,
                navText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive : {
                    1200 : {
                        items: 3,
                        slideBy: 3
                    },
                    768 : {
                        items: 3,
                        slideBy: 3
                    },
                    640 : {
                        items: 1,
                        slideBy: 1
                    },
                    480 : {
                        items: 1,
                        slideBy: 1
                    },
                    320 : {
                        items: 1,
                        slideBy: 1
                    }
                }
            });
        });

        // Testimonial
        $('.owlTestimonial').each(function() {
            var testimonial = $(this);
            testimonial.owlCarousel({
                autoplay: testimonial.data('autoplay'),
                autoplayTimeout: testimonial.data('speed'),
                autoHeight: true,
                loop: true,
                margin: 30,
                items: 1
            });
        });

        // Team
        $('.owlTeamMembers').each(function() {
            var team = $(this);
            team.owlCarousel({
                autoplay: team.data('autoplay'),
                autoplayTimeout: team.data('speed'),
                margin: 30,
                loop: true,
                responsive : {
                    1200 : {
                        items: 3
                    },
                    768 : {
                        items: 2
                    },
                    640 : {
                        items: 2
                    },
                    480 : {
                        items: 1
                    },
                    320 : {
                        items: 1
                    }
                }
            });
        });

        // Team - circle
        $('.owlTeamMembers-circle').each(function() {
            var circleTeam = $(this);
            circleTeam.owlCarousel({
                autoplay: circleTeam.data('autoplay'),
                autoplayTimeout: circleTeam.data('speed'),
                loop: true,
                margin: 60,
                nav: true,
                navText : ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
                responsive : {
                    1200 : {
                        items: 4,
                        slideBy: 4
                    },
                    768 : {
                        items: 4,
                        slideBy: 4
                    },
                    640 : {
                        items: 4,
                        slideBy: 4
                    },
                    480 : {
                        items: 3,
                        slideBy: 3
                    },
                    320 : {
                        items: 2,
                        slideBy: 2
                    }
                }
            });
        });

        // slider
        $('#owlSingleImage').each(function() {
            var slider = $(this);
            slider.owlCarousel({
                autoplay: slider.data('autoplay'),
                autoplayTimeout: slider.data('speed'),
                loop: true,
                margin: 0,
                items: 1
            });
        });

        // progress bar
        $('.progress').each(function() {
            $(this).appear(function() {
                $(this).find('.progress-bar').animate({
                    width : $(this).attr('data-percent')
                }, 200);
            });
        });

        // countTo
        var firstLoadCountEvent = true;
        $('.countTo').waypoint(function() {
            if (firstLoadCountEvent) {
                $('.countTo').countTo({
                    speed : 2000,
                    refreshInterval : 50,
                    formatter : function(value, options) {
                        return value.toFixed(options.decimals);
                    }
                });

                firstLoadCountEvent = false;
            }
        }, {
            offset : '80%'
        });

        // Add extra class for header layout two
        $('.header_container').next('section').addClass('header_container_next');

        var primaryNav = document.querySelector('.headroom');
        if(primaryNav){
            Stickyfill.add(primaryNav);
            var headroom = new Headroom(primaryNav, {
                offset: 105,
                tolerance: 10,
                classes:  {
                    initial: 'headroom--top'
                }
            });
            headroom.init();
        }

        var transparentNav = document.querySelector('.headroomTransparent');
        if(transparentNav) {
            var headroomTransparent = new Headroom(transparentNav, {
                offset: 105,
                tolerance: 10,
                classes:  {
                    initial: 'headroom--top'
                }
            });
            headroomTransparent.init();
        }

        // Multilevel dropdown fix
        $('.navbar-nav').on('mouseenter.HBnav click.HBnav', '.dropdown-submenu', function() {
            var submenuCon, rightOffset, element;
            element = $(this);
            submenuCon = element.parent();
            rightOffset = windowWidth - (submenuCon.offset().left + submenuCon.width());
            if (!element.hasClass('dropdown-submenu-left') && rightOffset < 200) {
                element.addClass('dropdown-submenu-left');
            }
        });

    });

}(jQuery));


