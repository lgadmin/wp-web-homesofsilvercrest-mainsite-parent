<?php
/**
 * Custom comment form for homebuilder
 *
 * @package homebuilder
 */

$commenter = wp_get_current_commenter();
$req       = get_option( 'require_name_email' );
$aria_req  = ( $req ? " aria-required='true'" : '' );

$fields =  array(

    'author' =>
    '<div class="row"><div class="col-md-6 "><div class="form-group"><label for="author" class="screen-reader-text">' . esc_html__( 'Name', 'homebuilder' ) . '</label> ' .
    '<input id="author" required name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) .
    '" placeholder="' . esc_attr__( 'Name*', 'homebuilder' ) .
    '" size="30"' . $aria_req . ' /></div></div>',

    'email' =>
    '<div class="col-md-6 "><div class="form-group"><label for="email" class="screen-reader-text">' . esc_html__( 'Email', 'homebuilder' ) . '</label> ' .
    '<input id="email" required name="email" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author_email'] ) .
    '" placeholder="' . esc_attr__( 'Email*', 'homebuilder' ) .
    '" size="30"' . $aria_req . ' /></div></div></div>',

);

$args = array(
    'class_submit'  => 'btn btn-read',
    'label_submit'  => esc_html__( 'Post Your Comment', 'homebuilder' ),

    'comment_notes_before' => '<p class="comment-notes ">' .
    esc_html__( 'Your email address will not be published.','homebuilder' ) . ( $req ? ' ' . esc_html__( 'Email and Name is required.', 'homebuilder' ) : '' ) .
    '</p>',

    'comment_field' =>  '<div class="row"><div class="col-md-12 form-group"><label for="comment" class="screen-reader-text">' . esc_html__( 'Comment', 'homebuilder' ) .
    '</label><div class="form-group"><textarea id="comment" name="comment" class="form-control" '.
    'placeholder="' . esc_attr__( 'Comment', 'homebuilder' ) . '" ' .
    'cols="45" rows="8">' .
    '</textarea></div></div></div>',

    'fields' => apply_filters( 'comment_form_default_fields', $fields ),
);

comment_form( $args );