<?php 
/**
 * The template for displaying single service.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package homebuilder
 */
$logo_id         = homebuilder_get_option( 'logo_default' );
$logo            = wp_get_attachment_image_src( $logo_id, 'large' );
$title_type      = homebuilder_get_option( 'page_title_layout', 'one' );
$service_logo_id = homebuilder_get_option( 'service_logo' );
$service_logo    = wp_get_attachment_image_src( $service_logo_id, 'large' );
get_header();
    if ( have_posts() ) {
        while ( have_posts() ) { the_post();
            $page_title     = get_the_title();
            $featured_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
            $featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
            set_query_var( 'page_title', $page_title );
            set_query_var( 'featured_image', $featured_image );
            get_template_part( "partials/page-title/{$title_type}" ); ?>
            <section class="p-v-xxl">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-md-push-8 m-b-xxl">
                            <ul class="nav nav-pills nav-stacked nav_services">
                                <li class="text-center p-v-xxl">
                                    <img class="logo_wild" src="<?php echo ( $service_logo[0] != '' ? $service_logo[0] : $logo[0] ); ?>" alt="<?php echo bloginfo('name'); ?>" />
                                </li>
                                <?php 
                                $service_id = get_the_ID();
                                homebuilder_all_services( $service_id ); ?>
                                <li></li>
                            </ul>
                        </div>
                        <div class="col-md-8 col-md-pull-4 m-v-xxl">
                            <div class="row">
                                <div class="col-md-12">
                                    <div><?php echo the_content(); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<?php  }    }
get_footer(); ?>