<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package homebuilder
 */

get_header();
$date_format = homebuilder_get_option( 'date_format', 'F j, Y ' );
$prev_link   = homebuilder_get_previous_post_link();
$next_link   = homebuilder_get_next_post_link();
$prev_title  = homebuilder_get_previous_post_title();
$next_title  = homebuilder_get_next_post_title();
$page_title     = homebuilder_get_option( 'blog_title', esc_html__( 'BLOG', 'homebuilder' ) );
$title_type     = homebuilder_get_option( 'post_title_layout', 'one' );
$blgo_bg_id     = homebuilder_get_option( 'blog_bg' );
$featured_image = wp_get_attachment_image_src( $blgo_bg_id, 'homebuilder-lg-soft' );
$featured_image = ( isset( $featured_image[0] ) ? $featured_image[0] : "" );
set_query_var( 'page_title', $page_title ); 
set_query_var( 'featured_image', $featured_image ); 
get_template_part( "partials/page-title/{$title_type}" ); ?>

<section class="post-content-<?php the_ID(); ?>">
	<?php
	while ( have_posts() ) { the_post(); ?>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php if( has_post_thumbnail() ){ ?>
                        <div class="m-b-xxl m-t-xxl featured-image">
                            <?php the_post_thumbnail( 'large' ); ?>
                        </div>
                    <?php } ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class( 'm-b-xxl' ); ?>>
                        <div class="p-v-xl">
                            <span class=""><a href="<?php the_permalink(); ?>"><?php the_time( $date_format ); ?></a></span>
                            <div class="section_heading m-t-sm">
                                <h1 class="title"><?php the_title() ?></h1>
                                <div class="sub_title text-md m-t-sm m-b-xl">
                                <?php
                                    //retrive author link
                                    echo homebuilder_author_link(); 
                                    //retrive categories
                                    if ( !empty( $categories ) ) {  homebuilder_get_post_categories( $categories ); };
                                    //retrive comments number
                                    homebuilder_post_comments_number(); 
                                ?>
                                </div>
                            </div>
                            <div><?php the_content(); ?></div>
                        </div>
                    </article>

                    <nav class="m-t-xxl m-b-xxl p-b-xxl">
                        <ul class="pager">
                            <li class="previous <?php echo homebuilder_check_link_status( $prev_title ); ?>">
                                <a href="<?php echo esc_url( $prev_link ); ?>" title="<?php printf( esc_attr__( 'Previous post: %s', 'homebuilder' ), $prev_title ); ?>"><i class="fa fa-long-arrow-left"></i> <?php echo esc_html( $prev_title ); ?></a>
                            </li>
                            <li class="next <?php echo homebuilder_check_link_status( $next_title ); ?>">
                                <a href="<?php echo esc_url( $next_link ); ?>" title="<?php printf( esc_attr__( 'Next post: %s', 'homebuilder' ), $next_title ); ?>"><?php echo esc_html( $next_title ); ?> <i class="fa fa-long-arrow-right"></i></a>
                            </li>
                        </ul>
                    </nav>
                    <?php
                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) {
                        echo '<div class="post-comment-block">';
                        comments_template();
                        echo '</div>';
                    } ?>
                </div>
            </div>
        </div>
    </section>
	<?php } // End of the loop. ?>
</section> <!--.post-content<?php the_ID(); ?> -->
<?php get_footer(); ?>