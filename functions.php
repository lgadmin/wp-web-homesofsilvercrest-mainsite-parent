<?php
/**
 * homebuilder functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package homebuilder
 */


/*******************************************************************
 * Constants
 *******************************************************************/

/** Homebuilder theme settings field name */
defined( 'CS_OPTION' ) || define( 'CS_OPTION', '_homebuilder_options' );

/** Homebuilder version */
define( 'HOMEBUILDER_VERSION', '1.1.0' );

/**
 * Directory paths
 *******************************************************************/

/** Homebuilder root directory path */
define( 'HOMEBUILDER_ROOT', trailingslashit( get_template_directory() ) );

/** Homebuilder includes directory path */
define( 'HOMEBUILDER_INCLUDES_DIR', trailingslashit( HOMEBUILDER_ROOT . 'includes' ) );

/** Homebuilder vendors directory path */
define( 'HOMEBUILDER_VENDORS_DIR', trailingslashit( HOMEBUILDER_ROOT . 'vendors' ) );

/** Homebuilder admin directory path */
define( 'HOMEBUILDER_ADMIN_DIR', trailingslashit( HOMEBUILDER_ROOT . 'admin' ) );

/**
 * Directory uri
 *******************************************************************/

/** Homebuilder root uri */
define( 'HOMEBUILDER_URI', trailingslashit( get_template_directory_uri() ) );

/** Homebuilder assets uri */
define( 'HOMEBUILDER_ASSETS_URI', trailingslashit( HOMEBUILDER_URI . 'assets' ) );

/** Homebuilder js uri */
define( 'HOMEBUILDER_JS_URI', trailingslashit( HOMEBUILDER_ASSETS_URI . 'js' ) );

/** Homebuilder css uri */
define( 'HOMEBUILDER_CSS_URI', trailingslashit( HOMEBUILDER_ASSETS_URI . 'css' ) );

/** Homebuilder vendor assets uri */
define( 'HOMEBUILDER_VENDOR_ASSETS_URI', trailingslashit( HOMEBUILDER_ASSETS_URI . 'vendor' ) );


 /*******************************************************************
 * Vendors
 *******************************************************************/

/** Breadcrumb Tail */
require_once HOMEBUILDER_VENDORS_DIR . 'class-tgm-plugin-activation.php';

/** Codestar Framework */
require_once HOMEBUILDER_VENDORS_DIR . 'cs-framework/cs-framework.php';

/** Register Custom Navigation Walker */
require_once HOMEBUILDER_VENDORS_DIR . 'class.bootstrap-navwalker.php';

/** Breadcrumb Tail */
require_once HOMEBUILDER_VENDORS_DIR . 'breadcrumb-trail.php';

/*******************************************************************
 * Helpers
 *******************************************************************/

/** Custom template tags for this theme. */
require HOMEBUILDER_INCLUDES_DIR . 'template-tags.php';

/** Custom helper functions. */
require HOMEBUILDER_INCLUDES_DIR . 'functions.helper.php';

/** Font Stack Generator Functions */
require HOMEBUILDER_INCLUDES_DIR . 'functions.font.php';

/*******************************************************************
 * Theme Setup
 *******************************************************************/

/** Styles Enqueue*/
require HOMEBUILDER_INCLUDES_DIR . 'class.theme-setup.php';

/** Admin Assets Enqueue*/
require HOMEBUILDER_INCLUDES_DIR . 'class.admin-assets.php';

/** Styles Enqueue*/
require HOMEBUILDER_INCLUDES_DIR . 'class.styles.php';

/** Scripts Enqueue*/
require HOMEBUILDER_INCLUDES_DIR . 'class.scripts.php';

/** Sidebars Register*/
require HOMEBUILDER_INCLUDES_DIR . 'class.sidebars.php';

/** Setup Theme Options */
require HOMEBUILDER_INCLUDES_DIR . 'class.theme-options.php';

/** Fonts Enqueue */
require HOMEBUILDER_INCLUDES_DIR . 'class.fonts.php';




/*******************************************************************
 * Custom & others
 *******************************************************************/

/**Custom functions that act independently of the theme templates.*/
require HOMEBUILDER_INCLUDES_DIR . 'extras.php';

/**Customizer additions.*/
require HOMEBUILDER_INCLUDES_DIR . 'customizer.php';

/**Load Jetpack compatibility file.*/
require HOMEBUILDER_INCLUDES_DIR . 'jetpack.php';

/**custom filters.*/
require HOMEBUILDER_INCLUDES_DIR . 'functions.filter.php';

/**Required plugins*/
require HOMEBUILDER_INCLUDES_DIR . 'class.required-plugins.php';

/**Social widget class*/
require HOMEBUILDER_INCLUDES_DIR . 'class.social.php';

/**Address widget class*/
require HOMEBUILDER_INCLUDES_DIR . 'class.address.php';


/*******************************************************************
 * HomeBuilder Demo
 *******************************************************************/
require HOMEBUILDER_INCLUDES_DIR . 'class.demo-controller.php';